# vim: syntax=python tabstop=4 expandtab
# coding: utf-8

# Rabema pipeline


# === Configuration

configfile: "config.json"


# === Rules

include: "rules/genbank.rules"
include: "rules/sra.rules"
include: "rules/samfiles.rules"
include: "rules/rabema.rules"
include: "rules/tex.rules"

include: "rules/razers3.rules"
include: "rules/yara.rules"
include: "rules/bowtie2.rules"
include: "rules/bwa.rules"


# === Functions

def get_references():
    return [row[0] for row in config["jobs"]]

def get_reads():
    return [row[1] for row in config["jobs"]]

def get_limits():
    return [str(row[2]) for row in config["jobs"]]

def expand_jobs(pattern, **kwargs):
    jobs = []
    for reference, reads, limit in config["jobs"]:
        jobs.extend(expand(pattern, reference=reference, reads=reads, limit=limit, **kwargs))
    return jobs


# === Stages

rule reference:
    input:
        expand("data/{reference}.fasta",
                reference=get_references())

rule index:
    input:
        expand("data/{reference}.{mapper}",
                reference=get_references(),
                mapper=config["mappers"].keys())

rule reads:
    input:
        expand("data/{reads}_{limit}_se.fastq.gz",
                zip,
                reads=get_reads(),
                limit=get_limits())

rule map:
    input:
        expand_jobs("data/{reads}_{limit}_se.{reference}.{mapper}.unsorted.bam",
                    mapper=config["mappers"].keys())

rule gold:
    input:
        expand_jobs("data/{reads}_{limit}_se.{reference}.{gold}.{errors}.gsi",
                    gold=config["gold"],
                    errors="5")

rule evaluate:
    input:
        expand_jobs("data/{reads}_{limit}_se.{reference}.{mapper}.{gold}.{errors}.{category}.rabema_report_tsv",
                    mapper=config["mappers"].keys(),
                    gold=config["gold"],
                    errors="5",
                    category="all-best")

rule report:
    input:
        expand_jobs("data/{reads}_{limit}_se.{reference}.{gold}.{errors}.{category}.pdf",
                    gold=config["gold"],
                    errors="5",
                    category="all-best")
